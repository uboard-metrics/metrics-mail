package br.com.mail.uboard.config.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import br.com.mail.uboard.exception.SendMailException;
import br.com.mail.uboard.model.MailDTO;
import br.com.mail.uboard.service.MailService;
import jakarta.validation.Valid;

@Component
public class SendMailConsumer {

	private static final Logger LOGGER = LoggerFactory.getLogger(SendMailConsumer.class);
	private static final String SEND_MAIL_TOPIC = "send-mail-topic";

	private MailService mailService;

	public SendMailConsumer(MailService mailService) {
		this.mailService = mailService;
	}

	@KafkaListener(groupId = "mail-group", topics = SEND_MAIL_TOPIC, containerFactory = "jsonContainerFactory")
	public void send(@Payload @Valid MailDTO mailDTO) throws Exception {
		try {
			LOGGER.debug("Sending mail from user: " + mailDTO.getFrom());
			this.mailService.sendMail(mailDTO);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new SendMailException(e.getMessage());
		}
	}

}
