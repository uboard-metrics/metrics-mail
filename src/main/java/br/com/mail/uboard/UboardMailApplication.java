package br.com.mail.uboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafka;

@EnableKafka
@SpringBootApplication
public class UboardMailApplication {

	public static void main(String[] args) {
		SpringApplication.run(UboardMailApplication.class, args);
	}

}
