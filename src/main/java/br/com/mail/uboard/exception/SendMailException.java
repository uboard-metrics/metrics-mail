package br.com.mail.uboard.exception;

public class SendMailException extends Exception {

	private static final long serialVersionUID = 1L;

	public SendMailException() {

	}

	public SendMailException(String message) {
		super(message);
	}

}
