FROM openjdk:17-alpine
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} uboard-mail-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java", "-jar", "uboard-mail-0.0.1-SNAPSHOT.jar"]